[![Build Status](https://gitlab.com/plantd/go-plantd/badges/master/build.svg)](https://gitlab.com/plantd/go-plantd/commits/master)
[![Coverage Report](https://gitlab.com/plantd/go-plantd/badges/master/coverage.svg)](https://gitlab.com/plantd/go-plantd/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plantd/go-plantd)](https://goreportcard.com/report/gitlab.com/plantd/go-plantd)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

---

# Plantd Go Library

Currently in beta, API subject to breaking changes.

Components shared across various services and tools.
