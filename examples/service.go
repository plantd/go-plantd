package main

import (
	"fmt"

	pl "gitlab.com/plantd/go-plantd/service"
)

type getConfiguration struct {
	name string
}

func main() {
	service := pl.NewService("echo", "tcp://localhost:5555")

	service.RegisterCallback("get-configuration", &getConfiguration{name: "get-configuration"})

	go service.Run()

	for done := false; !done {
		select {
		case msg := <-msgCh:
			// ...
		case err := <-errCh:
			if err != nil {
				fmt.Error(err)
			}
			done = true
		}
	}
}

func (g *getConfiguration) Execute() {
	fmt.Println(g.name)
}
