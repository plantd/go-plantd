package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type Envelope struct {
	Type string      `json:"type"`
	Msg  interface{} `json:"msg,omitempty"`
}

type ReadStatusRequest struct{}

type ReadStatusResponse struct {
	Properties map[string]interface{} `json:"properties,omitempty"`
}

func (r *ReadStatusRequest) ToJSON() (data []byte, err error) {
	data, err = json.Marshal(r)
	if err != nil {
		return nil, err
	}
	return
}

func (r *ReadStatusRequest) FromJSON(data []byte) (err error) {
	err = json.Unmarshal(data, &r)
	if err != nil {
		return err
	}
	return
}

func (r *ReadStatusRequest) MarshalJSON() ([]byte, error) {
	type Alias ReadStatusRequest
	return json.Marshal(&struct {
		Type string `json:"type"`
		*Alias
	}{
		Type:  "read-status-request",
		Alias: (*Alias)(r),
	})
}

// XXX: alternate method
/*
 *func (r *ReadStatusRequest) MarshalJSON() (data []byte, err error) {
 *    return json.Marshal(map[string]string{
 *        "type": "read-status-request",
 *    })
 *}
 */

func (r *ReadStatusRequest) UnmarshalJSON(data []byte) error {
	type Alias ReadStatusRequest
	aux := &struct {
		Type string `json:"type"`
		*Alias
	}{
		Alias: (*Alias)(r),
	}
	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}
	return nil
}

// XXX: alternate method
func (e *Envelope) UnmarshalJSON() (data []byte, err error) {
	var objMap map[string]*json.RawMessage
	err := json.Unmarshal(b, &objMap)
	if err != nil {
		return err
	}

	var rawMessagesForEnvelopes []*json.RawMessage
	err = json.Unmarshal(*objMap["things"], &rawMessagesForEnvelopes)
	if err != nil {
		return err
	}

	// Let's add a place to store our de-serialized Plant and Animal structs
	ce.Things = make([]Envelope, len(rawMessagesForEnvelopes))

	var m map[string]string
	for index, rawMessage := range rawMessagesForEnvelopes {
		err = json.Unmarshal(*rawMessage, &m)
		if err != nil {
			return err
		}

		// XXX: here - not done
		// Depending on the type, we can run json.Unmarshal again on the same byte slice
		// But this time, we'll pass in the appropriate struct instead of a map
		/*
		 *if m["type"] == "read-status-request" {
		 *    var p Plant
		 *    err := json.Unmarshal(*rawMessage, &p)
		 *    if err != nil {
		 *        return err
		 *    }
		 *    // After creating our struct, we should save it
		 *    ce.Things[index] = &p
		 *} else if m["type"] == "animal" {
		 *    var a Animal
		 *    err := json.Unmarshal(*rawMessage, &a)
		 *    if err != nil {
		 *        return err
		 *    }
		 *    // After creating our struct, we should save it
		 *    ce.Things[index] = &a
		 *} else {
		 *    return errors.New("Unsupported type found!")
		 *}
		 */
	}

	// That's it!  We made it the whole way with no errors, so we can return `nil`
	return nil
}

func main() {
	s := Envelope{
		Type: "read-status-request",
		Msg:  ReadStatusRequest{},
	}

	buf, err := json.Marshal(s)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s\n", buf)

	props := make(map[string]interface{})
	props["status"] = "ready"
	props["priority"] = 10

	c := Envelope{
		Type: "read-status-response",
		Msg: ReadStatusResponse{
			Properties: props,
		},
	}

	buf, err = json.Marshal(c)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s\n", buf)

	rs := &ReadStatusRequest{}
	buf, _ = rs.ToJSON()
	fmt.Printf("%s\n", buf)

	rs = &ReadStatusRequest{}
	err = rs.FromJSON(buf)
	if err != nil {
		fmt.Printf("error: %s\n", err)
	}
}
