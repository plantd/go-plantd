package main

import (
	"bytes"
	//"encoding/json"
	"fmt"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	"github.com/golang/protobuf/jsonpb"
)

// TODO: convert this into unit tests

func main() {
	properties := make([]*pb.Property, 0)
	p1 := &pb.Property{
		Key:   "k",
		Value: "v",
	}
	p2 := &pb.Property{
		Key:   "l",
		Value: "w",
	}
	properties = append(properties, p1)
	properties = append(properties, p2)

	objects := make([]*pb.Object, 0)
	o1 := &pb.Object{
		Id:         "o1",
		Name:       "object-a",
		Properties: properties,
		Objects:    nil,
	}
	o2 := &pb.Object{
		Id:         "o2",
		Name:       "object-b",
		Properties: properties,
		Objects:    nil,
	}
	objects = append(objects, o1)
	objects = append(objects, o2)

	// Type messages

	property(properties[0])
	object(objects[0])
	configuration(properties, objects)
	job()
	// TODO
	//settings()
	//status()
	//channel()?
	//unit()?
	//module()
	//system()/plant()

	// Request / Response messages

	//configurationRequest()
	//configurationResponse()
	//settingsRequest()
	//settingsResponse()
	//statusRequest()
	//statusResponse()
	//channelRequest()
	//channelListResponse()
	//moduleRequest()
	//moduleListResponse()
	//jobListResponse()
	//jobStatusResponse()
}

// Using these with a message envelope is probably the way to go.
// The envelope adds a type and msg field where type is the message name,
// and msg is the content of the output of jsonpb. There needs to be
// something that holds what the request is because the ZeroMQ service
// receiving the serialized data won't know without a message type.

func property(prop *pb.Property) {
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, prop); err != nil {
		fmt.Errorf("%s\n", err)
	}

	p := pb.Property{}
	if err := jsonpb.Unmarshal(bytes.NewReader(b.Bytes()), &p); err != nil {
		fmt.Errorf("%s\n", err)
	}

	// this just uses json formatting so I can display with jq
	fmt.Printf("{\"%s\": \"%s\"}\n", p.Key, p.Value)
	fmt.Println(b.String())
}

func object(obj *pb.Object) {
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, obj); err != nil {
		fmt.Errorf("%s\n", err)
	}

	fmt.Println(b.String())
}

func configuration(props []*pb.Property, objects []*pb.Object) {
	config := &pb.Configuration{
		Id:         "0xDEADBEEF",
		Namespace:  pb.Configuration_ANALYZE, // FIXME: doesn't print when ACQUIRE is used
		Properties: props,
		Objects:    objects,
	}

	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, config); err != nil {
		fmt.Errorf("%s\n", err)
	}

	fmt.Println(b.String())
}

func job() {
	j := &pb.Job{
		Id:     "0x00080085",
		Status: pb.Job_READY,
	}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, j); err != nil {
		fmt.Errorf("%s\n", err)
	}

	fmt.Println(b.String())
}
