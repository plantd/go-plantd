package main

import (
	"fmt"

	"gitlab.com/plantd/go-plantd/message"
)

func main() {
	s := message.StatusRequest{}
	bytes, _ := s.ToJSON()
	fmt.Println(string(bytes))
}
