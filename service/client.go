package service

import (
	"bytes"
	"errors"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	"gitlab.com/plantd/go-zapi/mdp"
)

type Client struct {
	conn *mdp.Client
}

// Establish a connection using the ZeroMQ API device
func NewClient(endpoint string) (c *Client, err error) {
	conn, err := mdp.NewClient(endpoint)
	if err != nil {
		return nil, err
	}

	c = &Client{conn}

	return
}

func (c *Client) sendMessage(id, message string, in interface{}, out interface{}) error {
	req := make([]string, 2)
	req[0] = message

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in.(proto.Message)); err != nil {
		return err
	}

	// Send the message
	req[1] = b.String()
	_ = c.conn.Send(id, req...)
	// Wait for a reply
	reply, err := c.conn.Recv()
	if err != nil {
		return err
	}

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		return errors.New("didn't receive expected response")
	} else if len(reply) > 1 {
		// only handle first response for now
	}

	// FIXME: in some cases the body is in [0], others it's [1], and now there's a [2] case
	// TODO: search the array for the first valid json string and use that
	// TODO: could also search the array for the matching message as a form of confirmation

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[2])), out.(proto.Message)); err != nil {
		return err
	}

	return nil
}

// Request the configuration of a service
// TODO: v2 -> use Request
func (c *Client) GetConfiguration(id string) (*pb.ConfigurationResponse, error) {
	request := &pb.ConfigurationRequest{Id: id}
	response := &pb.ConfigurationResponse{}
	if err := c.sendMessage(id, "get-configuration", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request the status of a service
// TODO: v2 -> use Request or StatusRequest
func (c *Client) GetStatus(id string) (*pb.StatusResponse, error) {
	request := &pb.ModuleRequest{Id: id}
	response := &pb.StatusResponse{}
	if err := c.sendMessage(id, "get-status", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request information about a job that's running or queued
// TODO: v2 -> use JobRequest
func (c *Client) GetJob(id, jobId string) (*pb.JobResponse, error) {
	request := &pb.ModuleJobRequest{
		Id:    id,
		JobId: jobId,
	}
	response := &pb.JobResponse{}
	if err := c.sendMessage(id, "get-job", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request all jobs running or queued on a service
// TODO: v2 -> use Request
func (c *Client) GetJobs(id string) (*pb.JobsResponse, error) {
	request := &pb.Empty{}
	response := &pb.JobsResponse{}
	if err := c.sendMessage(id, "get-jobs", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request information about a single module
// TODO: v2 -> use Request
func (c *Client) GetModule(id string) (*pb.ModuleResponse, error) {
	request := &pb.ModuleRequest{Id: id}
	response := &pb.ModuleResponse{}
	if err := c.sendMessage(id, "get-module", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request all modules registered with the service
// TODO: v2 -> use Request
func (c *Client) GetModules(id string) (*pb.ModulesResponse, error) {
	request := &pb.Empty{}
	response := &pb.ModulesResponse{}
	if err := c.sendMessage(id, "get-modules", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request the settings of a service
// TODO: v2 -> use Request
func (c *Client) GetSettings(id string) (*pb.SettingsResponse, error) {
	request := &pb.SettingsRequest{}
	response := &pb.SettingsResponse{}
	if err := c.sendMessage(id, "get-settings", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request the active job of a service
// TODO: v2 -> use Request
func (c *Client) GetActiveJob(id string) (*pb.JobResponse, error) {
	request := &pb.ModuleRequest{Id: id}
	response := &pb.JobResponse{}
	if err := c.sendMessage(id, "get-active-job", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request cancel for a job that's running or queued on a service
// TODO: v2 -> use JobRequest
func (c *Client) CancelJob(id, jobId string) (*pb.JobResponse, error) {
	request := &pb.ModuleJobRequest{
		Id:    id,
		JobId: jobId,
	}
	response := &pb.JobResponse{}
	if err := c.sendMessage(id, "cancel-job", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request a job submit to a service
// TODO: v2 -> use JobRequest
func (c *Client) SubmitJob(id, jobName, jobValue string, jobProperties map[string]string) (*pb.JobResponse, error) {
	//request := &pb.JobRequest{Id: id}
	var properties []*pb.Property
	for key, value := range jobProperties {
		properties = append(properties, &pb.Property{Key: key, Value: value})
	}
	request := &pb.ModuleJobRequest{
		Id:            id,
		JobId:         jobName,
		JobValue:      jobValue,
		JobProperties: properties,
	}
	response := &pb.JobResponse{}
	if err := c.sendMessage(id, "submit-job", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request an event be raised
/*
 *func (c *Client) SubmitEvent(id string, event int) (*pb.EventResponse, error) {
 *}
 */

// Request all events that a service can raise
// TODO: v2 -> use Request
func (c *Client) AvailableEvents(id string) (*pb.EventsResponse, error) {
	request := &pb.ModuleRequest{Id: id}
	response := &pb.EventsResponse{}
	if err := c.sendMessage(id, "available-events", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request a single property from a service
func (c *Client) GetProperty(id, key string) (*pb.PropertyResponse, error) {
	request := &pb.PropertyRequest{
		Id:  id,
		Key: key,
	}
	response := &pb.PropertyResponse{}
	if err := c.sendMessage(id, "get-property", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request a list of properties from a service
func (c *Client) GetProperties(id string, keys []string) (*pb.PropertiesResponse, error) {
	var properties []*pb.Property
	for _, key := range keys {
		properties = append(properties, &pb.Property{Key: key})
	}
	request := &pb.PropertiesRequest{
		Id:         id,
		Properties: properties,
	}
	response := &pb.PropertiesResponse{}
	if err := c.sendMessage(id, "get-properties", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request a change of a single property by a service
func (c *Client) SetProperty(id, key, value string) (*pb.PropertyResponse, error) {
	request := &pb.PropertyRequest{
		Id:    id,
		Key:   key,
		Value: value,
	}
	response := &pb.PropertyResponse{}
	if err := c.sendMessage(id, "set-property", request, response); err != nil {
		return nil, err
	}
	return response, nil
}

// Request a change of multiple properties by a service
func (c *Client) SetProperties(id string, properties map[string]string) (*pb.PropertiesResponse, error) {
	var props []*pb.Property
	for key, value := range properties {
		props = append(props, &pb.Property{Key: key, Value: value})
	}
	request := &pb.PropertiesRequest{
		Id:         id,
		Properties: props,
	}
	response := &pb.PropertiesResponse{}
	if err := c.sendMessage(id, "set-properties", request, response); err != nil {
		return nil, err
	}
	return response, nil
}
