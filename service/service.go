package service

type Service struct {
	serviceName string
	endpoint    string
	handler     *Handler
	jobQueue    *JobQueue
	jobs        map[string]*Job
}

func NewService(serviceName, endpoint string) *Service {
	handler := NewHandler()
	jobQueue := NewJobQueue()

	return &Service{
		serviceName: serviceName,
		endpoint:    endpoint,
		handler:     handler,
		jobQueue:    jobQueue,
		jobs:        make(map[string]*Job),
	}
}

func (s *Service) Run() (err error) {
	// TODO: add error channels
	go s.handler.Start(s.endpoint, s.serviceName)
	go s.jobQueue.Start()

	err = nil
	return
}

func (s *Service) Shutdown() (err error) {
	err = s.handler.Stop()
	err = s.jobQueue.Stop()

	// only returns last error for now
	return
}

func (s *Service) RegisterJob(job *Job) {
	//s.jobs[job.GetName()] = job
	s.jobs["foo"] = job
}

func (s *Service) RegisterCallback(name string, callback HandlerCallback) {
	s.handler.AddCallback(name, callback)
}
