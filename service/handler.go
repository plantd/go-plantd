package service

import (
	"errors"

	"gitlab.com/plantd/go-zapi/mdp"

	"github.com/sirupsen/logrus"
)

type Handler struct {
	running   bool
	cancelled bool
	callbacks map[string]HandlerCallback
}

type HandlerCallback interface {
	Execute(msgBody string) ([]byte, error)
}

func NewHandler() *Handler {
	return &Handler{
		running:   false,
		cancelled: false,
		callbacks: make(map[string]HandlerCallback),
	}
}

func (h *Handler) Start(endpoint, service string) error {
	worker, _ := mdp.NewWorker(endpoint, service)
	var err error
	var request, reply []string

	for {
		request, err = worker.Recv(reply)
		if err != nil {
			return err
		}

		if len(request) == 0 {
			return errors.New("request has zero parts")
		}

		msgType := request[0]
		logrus.Info(msgType)

		// Reset reply
		reply = []string{}
		multiPart := false

		// TODO: return errors in an error channel
		// TODO: get rid of logging methods, should be at a higher level
		// TODO: could use something like h.callbacks[msgType].Execute()
		// TODO: would need to check if message is available first
		// TODO: find a way to execute methods that have been provided as callbacks
		for _, part := range request[1:] {
			logrus.WithFields(logrus.Fields{
				"procedure": msgType,
			}).Infof("received message: %s", part)
			var data []byte
			switch msgType {
			case "get-configuration",
				"get-status",
				"get-settings",
				"get-job",
				"get-jobs",
				"get-active-job",
				"cancel-job",
				"submit-job",
				"submit-event",
				"available-events",
				"get-property",
				"set-property",
				"get-properties",
				"set-properties":
				// TODO: validate message type
				if data, err = h.callbacks[msgType].Execute(part); err != nil {
					logrus.WithFields(logrus.Fields{
						"type": msgType,
					}).Warnf("message failed: %s", err)
					break
				}
			case "shutdown":
				if !multiPart {
					if data, err = h.Shutdown(part); err != nil {
						logrus.WithFields(logrus.Fields{
							"type": msgType,
						}).Warnf("message failed: %s", err)
						break
					}
				}
				multiPart = true
			default:
				// TODO:
				logrus.Error("invalid message type provided")
			}

			// Append
			reply = append(reply, string(data))
			logrus.Debug(reply)
		}
	}

	return nil
}

func (h *Handler) Stop() error {
	// stop go routine
	return nil
}

func (h *Handler) Shutdown(msgBody string) ([]byte, error) {
	// do something
	return []byte("{}"), nil
}

func (h *Handler) AddCallback(name string, callback HandlerCallback) {
	h.callbacks[name] = callback
}
