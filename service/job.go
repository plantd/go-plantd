package service

type Job interface {
	GetName() string
	GetPriority() int
	Start() error
	Stop() error
	Cancel() error
	Pause() error
	Resume() error
}
