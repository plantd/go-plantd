package service

type JobQueue struct {
	running   bool
	cancelled bool
}

func NewJobQueue() *JobQueue {
	return &JobQueue{}
}

func (h *JobQueue) Start() error {
	// launch go routine that handles jobs
	return nil
}

func (h *JobQueue) Stop() error {
	// stop go routine
	return nil
}
