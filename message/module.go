package message

import (
	"encoding/json"
	"errors"

	"gitlab.com/plantd/go-plantd/model"
	pb "gitlab.com/plantd/go-plantd/proto/v1"
)

func CreateModuleListRequest() ([]byte, error) {
	var props []model.Property
	props = append(props, model.Property{
		Key:   "module",
		Value: "list",
	})

	body := model.RequestBody{
		Type:       "request-body",
		Properties: props,
	}

	msg := model.RequestMessage{
		Type: "request-message",
		Body: body,
	}

	// Convert the message into a byte representation of the message
	bytes, err := json.Marshal(&msg)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func CreateModuleListResponse(data []byte) (*pb.ModuleListReply, error) {
	// Receive the bytes and marshal them into response-message
	u := &model.Unpacker{}
	if err := json.Unmarshal(data, u); err != nil {
		return nil, err
	}

	var resp = u.Data.(*model.ResponseMessage)
	if resp.Type != "response-message" {
		return nil, errors.New("received wrong message type")
	}

	modules := []*pb.Module{}

	// TODO: add modules

	return &pb.ModuleListReply{
		Modules: modules,
	}, nil
}

func CreateModuleListAllRequest() ([]byte, error) {
	var props []model.Property
	props = append(props, model.Property{
		Key:   "module",
		Value: "list-all",
	})

	body := model.RequestBody{
		Type:       "request-body",
		Properties: props,
	}

	msg := model.RequestMessage{
		Type: "request-message",
		Body: body,
	}

	// Convert the message into a byte representation of the message
	bytes, err := json.Marshal(&msg)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func CreateModuleListAllResponse(data []byte) (*pb.ModuleListReply, error) {
	// Receive the bytes and marshal them into response-message
	u := &model.Unpacker{}
	if err := json.Unmarshal(data, u); err != nil {
		return nil, err
	}

	var resp = u.Data.(*model.ResponseMessage)
	if resp.Type != "response-message" {
		return nil, errors.New("received wrong message type")
	}

	modules := []*pb.Module{}

	// TODO: add modules

	return &pb.ModuleListReply{
		Modules: modules,
	}, nil
}
