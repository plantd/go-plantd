package message

import (
	"encoding/json"
	"errors"

	"gitlab.com/plantd/go-plantd/model"
	pb "gitlab.com/plantd/go-plantd/proto/v1"
)

/*
 *func StatusRequestJSON(request model.Request) ([]byte, error) {
 *}
 *
 *func StatusResponseJSON(response model.Response) ([]byte, error) {
 *}
 *
 *func StatusRequestProtobuf(request model.Request) (*pb.ReadStatusMessage, error) {
 *}
 *
 *func StatusResponseProtobuf(request model.Request) (*pb.Status, error) {
 *}
 *
 *func StatusRequestFromJSON([]byte) () {
 *}
 */

type StatusRequest struct{}

type StatusRequestMessage struct {
	name string
	body map[string]interface{} `json:"name,omitempty"`
}

func NewStatusRequest() (req StatusRequestMessage) {
	req = StatusRequestMessage{
		name: "request-status",
	}
	return
}

func (s StatusRequestMessage) Name() string {
	return s.name
}

func (s StatusRequestMessage) Body() map[string]interface{} {
	return s.body
}

func (s *StatusRequest) ToJSON() (bytes []byte, err error) {
	props := []model.Property{
		model.Property{
			Key:   "status",
			Value: "read",
		},
	}

	body := model.RequestBody{
		Type:       "request-body",
		Properties: props,
	}

	msg := model.RequestMessage{
		Type: "request-message",
		Body: body,
	}

	// Convert the message into a byte representation of the message
	bytes, err = json.Marshal(&msg)
	if err != nil {
		return nil, err
	}

	return
}

func (s *StatusRequest) FromJSON(bytes []byte) (err error) {
	// This has no fields currently, nothing to do
	return nil
}

// Create a request message using `request` as the type of status check, eg. read
//
// XXX: should this receive a protobuf message? if so the type could come from it
// XXX: should this return a []byte?
func CreateStatusRequest(request string) ([]byte, error) {
	// TODO: define types possible for request and check here before creating

	var props []model.Property
	props = append(props, model.Property{
		Key:   "status",
		Value: request,
	})

	body := model.RequestBody{
		Type:       "request-body",
		Properties: props,
	}

	msg := model.RequestMessage{
		Type: "request-message",
		Body: body,
	}

	// Convert the message into a byte representation of the message
	bytes, err := json.Marshal(&msg)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func CreateStatusResponse(data []byte) (*pb.Status, error) {
	// Receive the bytes and marshal them into response-message
	u := &model.Unpacker{}
	if err := json.Unmarshal(data, u); err != nil {
		return nil, err
	}

	var resp = u.Data.(*model.ResponseMessage)
	if resp.Type != "response-message" {
		return nil, errors.New("received wrong message type")
	}

	var functional = false
	for _, prop := range resp.Body.Properties {
		if prop.Key == "status" && prop.Value == "ready" {
			functional = true
		}
	}

	return &pb.Status{
		Functional: functional,
	}, nil
}

// -- new things

type ReadStatus struct{}
