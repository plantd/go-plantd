package message

import (
	"encoding/json"
	"errors"

	"gitlab.com/plantd/go-plantd/model"
	pb "gitlab.com/plantd/go-plantd/proto/v1"
)

func CreateSettingsRequest(request string) ([]byte, error) {
	// TODO: define types possible for request and check here before creating

	var props []model.Property
	props = append(props, model.Property{
		Key:   "settings",
		Value: request,
	})

	body := model.RequestBody{
		Type:       "request-body",
		Properties: props,
	}

	msg := model.RequestMessage{
		Type: "request-message",
		Body: body,
	}

	// Convert the message into a byte representation of the message
	bytes, err := json.Marshal(&msg)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func CreateSettingsResponse(data []byte) (*pb.Settings, error) {
	// Receive the bytes and marshal them into response-message
	u := &model.Unpacker{}
	if err := json.Unmarshal(data, u); err != nil {
		return nil, err
	}

	var resp = u.Data.(*model.ResponseMessage)
	if resp.Type != "response-message" {
		return nil, errors.New("received wrong message type")
	}

	properties := map[string]string{}
	for _, prop := range resp.Body.Properties {
		properties[prop.Key] = prop.Value
	}

	return &pb.Settings{
		Properties: properties,
	}, nil
}
