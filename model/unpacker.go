package model

import (
	"encoding/json"
	"fmt"
)

type Unpacker struct {
	Data interface{}
}

// General process:
//
//  - unmarshal
//  - check that something was unmarshaled, if yes return
//  - check for an error other than type, if yes return
func (u *Unpacker) UnmarshalJSON(b []byte) error {
	// RequestMessage
	reqMsg := &RequestMessage{}
	err := json.Unmarshal(b, reqMsg)

	if err == nil && reqMsg.Type != "" && reqMsg.Type == "request-message" {
		u.Data = reqMsg
		return nil
	}

	if _, ok := err.(*json.UnmarshalTypeError); err != nil && !ok {
		return err
	}

	// ResponseMessage
	respMsg := &ResponseMessage{}
	err = json.Unmarshal(b, respMsg)

	if err == nil && respMsg.Type != "" && respMsg.Type == "response-message" {
		u.Data = respMsg
		return nil
	}

	if _, ok := err.(*json.UnmarshalTypeError); err != nil && !ok {
		return err
	}

	// RequestBody
	reqBody := &RequestBody{}
	err = json.Unmarshal(b, reqBody)

	if err == nil && reqBody.Type != "" && reqBody.Type == "request-body" {
		u.Data = reqBody
		return nil
	}

	if _, ok := err.(*json.UnmarshalTypeError); err != nil && !ok {
		return err
	}

	// ResponseBody
	respBody := &ResponseBody{}
	err = json.Unmarshal(b, respBody)

	if err == nil && respBody.Type != "" && respBody.Type == "response-body" {
		u.Data = respMsg
		return nil
	}

	if _, ok := err.(*json.UnmarshalTypeError); err != nil && !ok {
		return err
	}

	// Couldn't resolve type
	return nil
}

func (u *Unpacker) String() string {
	switch d := u.Data.(type) {
	case *RequestMessage:
		return fmt.Sprint("Request: ", d)
	case *ResponseMessage:
		return fmt.Sprint("Response: ", d)
	case *RequestBody:
		return fmt.Sprint("Request Body: ", d)
	case *ResponseBody:
		return fmt.Sprint("Response Body: ", d)
	}
	return ""
}
