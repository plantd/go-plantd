package model

type Property struct {
	Key   string `bson:"key"   json:"key"`
	Value string `bson:"value" json:"value"`
	//Value any.Any `bson:"value" json:"value"`
}
