module gitlab.com/plantd/go-plantd

require (
	github.com/go-kit/kit v0.9.0
	github.com/golang/protobuf v1.3.1
	github.com/pebbe/zmq4 v1.0.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	gitlab.com/plantd/go-zapi v0.0.0-20191011203435-babea2a4d192
	golang.org/x/net v0.0.0-20190213061140-3a22650c66bd
	google.golang.org/grpc v1.19.0
)

go 1.13
